#!/bin/bash

choice=$(fc-list | awk '{print $1}' | sed 's/://g' | dmenu -fn 'Monoid-10' -l 20 -p 'Fonts: ')
display "$choice"
